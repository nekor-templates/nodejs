import { Before, Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

describe('Поиск в Яндекс', () => {
  Given('я на главной странице Яндекс', () => {
    cy.visit('https://yandex.ru/');
  });

  describe('Подсказки поиска', () => {
    When(/^я набираю в поиске "github"$/, query => {
      cy.get('input#text').type('github');
    });

    Then(/^я вижу подсказки$/, () => {
      cy.get('.mini-suggest__popup-content')
        .find('.mini-suggest__item')
        .its('length')
        .then(length => {
          expect(length).to.be.greaterThan(5);
        });
    });
  });

  describe('Пустой поиск', () => {
    When(/^я нажимаю "Найти"$/, () => {
      cy.get('button[type="submit"]').click();
    });

    Then(/^я вижу сообщение "Задан пустой поисковый запрос"$/, () => {
      cy.get('.misspell__message').contains('Задан пустой поисковый запрос');
    });
  });

  describe('Простой поиск', () => {
    When(/^я набираю в поиске "github"$/, query => {
      cy.get('input#text').type('github');
    });

    When(/^нажимаю "Найти"$/, () => {
      cy.get('button[type="submit"]')
        .contains('Найти')
        .parents('form')
        .submit();
    });

    Then(/^я вижу результаты$/, () => {
      cy.get('h2.typo')
        .its('length')
        .then(length => {
          expect(length).to.be.greaterThan(5);
        });
    });
  });
});
