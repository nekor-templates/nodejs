import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps';

describe('Search Google', () => {
  Given(/^I'm at Google$/, () => {
    cy.visit('https://www.google.ru');
  });

  describe('Common search', () => {
    When(/^I type search word 'github'$/, query => {
      cy.get('input[type=text]').type('github');
    });

    And(/^Press 'Search'$/, title => {
      cy.get('input[type=submit]')
        .contains('Поиск в Google')
        .click();
    });

    Then(/^I have some results$/, () => {
      cy.get('#rso')
        .find('h3')
        .its('length')
        .then(length => {
          expect(length).to.be.greaterThan(5);
        });
    });
  });
});
