Feature: Search Google

  Background:
    Given I'm at Google

  Scenario: Common search
    When I type search word 'github'
    And Press 'Search'
    Then I have some results
